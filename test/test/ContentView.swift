//
//  ContentView.swift
//  test
//
//  Created by Emre on 10/01/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Let's create a conflict! Yey!")
                .padding()
            Text("Hello, world!")
                .padding()
            Text("Third element added")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
